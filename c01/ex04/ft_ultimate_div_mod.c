/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ultimate_div_mod.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jang <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/11/29 16:48:43 by jang              #+#    #+#             */
/*   Updated: 2022/11/29 19:37:42 by jang             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

//divides a by b
//result stored in int pointed by a
//remainder of division stored in int pointed by b

#include <unistd.h>

void	ft_ultimate_div_mod(int *a, int *b)
{
	int	a_value;

	a_value = *a;
	*a = *a / *b;
	*b = a_value % *b;
}

/*
#include <stdio.h>

int main(void)
{
	int	a;
	int	b;

	a = 21;
	b = 4;
	ft_ultimate_div_mod(&a, &b);
	printf("%d", a);
	printf("%d", b);
	return (0);
}
*/
