/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ft.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jang <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/11/28 10:43:51 by jang              #+#    #+#             */
/*   Updated: 2022/11/29 19:35:55 by jang             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

//pointer to int
//set value 42 to that int

void	ft_ft(int *nbr)
{
	*nbr = 42;
}

/*
#include <stdio.h>

int	main(void)
{
	int	*nbr;
	int	a;

	nbr = &a;
	ft_ft(nbr);
	printf("%d", a);
	return (0);
}
*/
