/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_swap.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jang <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/11/28 15:38:52 by jang              #+#    #+#             */
/*   Updated: 2022/11/28 17:13:51 by jang             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

//swap two integers whose
//address are entered as parameters

void	ft_putchar(char c)
{
	write(1, &c, 1);
}

void	ft_putnbr(int nb)
{
	if (nb == -2147483647)
	{
		ft_putchar('-');
		ft_putchar('2');
		nb = 147483647;
	}
	else if (nb < 0)
	{
		ft_putchar('-');
		nb *= -1;
	}
	else if (nb >= 10)
	{
		ft_putnbr(nb / 10);
		ft_putnbr(nb % 10);
	}
	else
	{
		ft_putchar(nb + 48);
	}
}

void	ft_swap(int *a, int *b)
{
	int	c;

	c = *a;
	*a = *b;
	*b = c;
}

/*
#include <stdio.h>

int	main(void)
{
	int	*a;
	int	*b;
	int	j;
	int	k;

	j = 3;
	k = 42;
	a = &j;
	b = &k;
	ft_swap(a, b);
	printf("%d", *a);
	printf("\n");
	printf("%d", *b);
	return(0);
}
*/
