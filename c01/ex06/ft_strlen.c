/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlen.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jang <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/11/29 18:04:37 by jang              #+#    #+#             */
/*   Updated: 2022/11/29 18:44:01 by jang             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

//create a function
//counts and returns no. of char in string

#include <unistd.h>

int	ft_strlen(char *str)
{
	int	i;

	i = 0;
	while (str[i] != '\0')
	{
		i++;
	}
	return (i);
}

/*
#include <stdio.h>

int	main(void)
{
	int	len;
	len = ft_strlen("halp!");
	printf("%d", len);
	return (0);
}
*/
