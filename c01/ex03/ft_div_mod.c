/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_div_mod.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jang <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/11/29 16:45:46 by jang              #+#    #+#             */
/*   Updated: 2022/11/29 16:56:40 by jang             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

//Divides parameters a by b and stores the result in the int
//pointed by div.
//stores the remainder of the division of a by
//b in the int pointed by mod

#include <unistd.h>

void	ft_div_mod(int a, int b, int *div, int *mod)
{
	*div = a / b;
	*mod = a % b;
}

/*
#include <stdio.h>

int main(void)
{
	int	*div;
	int	*mod;
	int	j;
	int	k;

	div = &j;
	mod = &k;
	ft_div_mod(21, 4, div, mod);
	printf("%d", j);
	printf("\n");
	printf("%d", k);
	return (0);
}
*/
