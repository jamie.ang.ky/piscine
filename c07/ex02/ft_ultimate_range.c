/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ultimate_range.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jang <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/12/17 17:28:54 by jang              #+#    #+#             */
/*   Updated: 2022/12/17 17:31:45 by jang             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

//func which allocates and displays an array of ints
//contain all values between min and max
// size of range should be retunred (or -1 on error).
// if min >= max, range will point to NULL n return 0.

#include<stdlib.h>

int	ft_ultimate_range(int **range, int min, int max)
{
	int	i;
	int	*arr;

	i = 0;
	if (min >= max)
	{
		*range = NULL;
		return (0);
	}
	arr = (int *)malloc(sizeof(int) * (max - min));
	if (arr == NULL)
		return (-1);
	while (min < max)
	{
		arr[i] = min;
		i++;
		min++;
	}
	*range = arr;
	return (i);
}
/*
#include <stdio.h>

int main(void)
{
	int	i;
	int j;
	int min;
	int max;
	int size;
	int *range = (void *)0;

	printf("main: min<max: original array pointer: %p\n", range);
	min = 5;
	max = 15;
	size = ft_ultimate_range(&range, min, max);
	printf("main: after function, %d any values?\n", size);
	i = 0;
	j = min;
	while (j < max)
	{
		printf("%d\n", range[i]);
		i++;
		j++;
	}
	printf("\n\n");
	printf("main2 min>max: original array pointer: %p\n", range);
	min = 25;
	max = 15;
	size = ft_ultimate_range(&range, min, max);
	printf("main2: pointer returned from function: %p\n",range);
	printf("main2: value from pointer returned from function: %p\n",range);

  return (0);
}
*/
/*
#include<stdio.h>
#include<unistd.h>
int	main(void)
{
	int	*tmp = NULL;
	int	**range = &tmp;
	int	*ptr = NULL;
	int	i;
	int	n;

	n = ft_ultimate_range(range, 'A', 'Z');
	i = 0;
	ptr = *range;
	while (i < n)
	{
		write(1, &ptr[i], 1);
		write(1, " ", 1);
		i++;
	}
	printf("%d", n);
	return (0);
}
*/