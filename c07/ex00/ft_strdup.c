/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strdup.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jang <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/12/17 14:40:31 by jang              #+#    #+#             */
/*   Updated: 2022/12/17 16:01:59 by jang             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

int	ft_strlen(char *src)
{
	int	i;

	i = 0;
	while (src[i] != '\0')
		i++;
	return (i);
}

char	*ft_strdup(char *src)
{
	int		i;
	char	*desc;

	i = 0;
	desc = (char *)malloc(sizeof(char) * (ft_strlen(src) + 1));
	if (desc == NULL)
		return (NULL);
	while (src[i] != '\0')
	{
		desc[i] = src[i];
		i++;
	}
	desc[i] = '\0';
	return (desc);
}

/*
#include <stdio.h>
int	main(void)
{
	char	src[] = "Duplicate this string using malloc.";

	printf("%s", ft_strdup(src));
	return (0);
}
*/
