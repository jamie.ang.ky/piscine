/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strjoin.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jang <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/12/18 12:54:39 by jang              #+#    #+#             */
/*   Updated: 2022/12/18 14:32:17 by jang             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

//write a function that concatenate all the strings pointed by strs
//separated by sep
//size is number of strings in strs
//if size is 0, return an empty string that you can free.

#include <stdlib.h>

int	ft_arrlen(int size, char **strs)
{
	int	i;
	int	j;
	int	total;

	i = 0;
	j = 0;
	total = 0;
	while (i <= (size - 1))
	{
		j = 0;
		while (strs[i][j] != '\0')
		{
			j++;
			total++;
		}
		i++;
	}
	return (total);
}

int	ft_strlen(char *sep)
{
	int	i;

	i = 0;
	while (sep[i] != '\0')
		i++;
	return (i);
}

char	*ft_strjoin(int size, char **strs, char *sep)
{
	int		i;
	int		j;
	int		ptr_i;
	char	*ptr;

	i = 0;
	ptr_i = 0;
	ptr = (char *)malloc(ft_arrlen(size, strs)
			+ (ft_strlen(sep)) * (size - 1) + 1);
	if (ptr == NULL)
		return (NULL);
	while (i < size)
	{
		j = 0;
		while (strs[i][j] != '\0')
			ptr[ptr_i++] = strs[i][j++];
		j = 0;
		while (sep[j] != '\0' && i < (size - 1))
			ptr[ptr_i++] = sep[j++];
		i++;
	}
	ptr[ptr_i] = '\0';
	return (ptr);
}
/*
#include <stdio.h>
#include <unistd.h>
int	main(void)
{
	char	*strs[] = {"This", "is", "a", "2", "D", "Array."};
	char	sep[] = "-----";
	int		size = 6;

	printf("Strs size is %d\n", ft_arrlen(size, strs));
	printf("Sep size is %d\n", ft_strlen(sep));
	write(1, ft_strjoin(size, strs, sep), 100);
	free(ft_strjoin(0, strs, sep));
	return (0);
}
*/