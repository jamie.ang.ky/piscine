/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_range.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jang <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/12/17 15:56:11 by jang              #+#    #+#             */
/*   Updated: 2022/12/17 15:59:22 by jang             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

//create function which returns an array of ints.
//should  contain all values between min and max
//min included, max excluded
// if min value is greater or equal to max, null pointer is returned.

#include <stdlib.h>

int	ft_arrlen(int min, int max)
{
	int	i;

	i = max - min;
	return (i);
}

int	*ft_range(int min, int max)
{
	int	i;
	int	*dest;

	i = 0;
	if (min >= max)
		return (0);
	dest = (int *)malloc(sizeof(int) * (ft_arrlen(min, max)));
	if (dest == NULL)
		return (NULL);
	while (i < ft_arrlen(min, max))
	{
		dest[i] = min + i;
		i++;
	}
	return (dest);
}
/*
#include <stdio.h>
int main(void)
{
	int *int_arr_p;
  int i;

	int_arr_p = ft_range(0, 1);
	i = 0;
	while (i < (1 - 0))
	{
		printf("%d,", int_arr_p[i]);
		i++;
	}

	printf("\n");
	int_arr_p = ft_range(0, 15);
	i = 0;
	while (i < (15 - 0))
	{
		printf("%d,", int_arr_p[i]);
		i++;
	}

	printf("\n");
	int_arr_p = ft_range(-10, 11);
	i = 0;
	while (i < (11 - - 10))
	{
		printf("%d,", int_arr_p[i]);
		i++;
	}

	printf("\n");
	int_arr_p = ft_range(-5, 12);
	i = 0;
	while (i < (12 - - 5))
	{
		printf("%d,", int_arr_p[i]);
		i++;
	}

	printf("\n");
	int_arr_p = ft_range(-2, 15);
	i = 0;
	while (i < (15 - - 2))
	{
		printf("%d,", int_arr_p[i]);
		i++;
	}
}
*/