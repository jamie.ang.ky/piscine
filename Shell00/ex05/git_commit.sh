#! /bin/bash
set -m
git log --format='%H' -n5

# --format '%H' condenses the default git log to only show commit hash
# -n5 displays number of commits last made
# commit hashes are values that are mathematical unique and act like an identifier
# when running the command without -m, the error 'no job control' appears.
# set -m enables job control allowing all processes run in a separate process group.
