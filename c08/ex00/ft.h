/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft.h                                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jang <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/12/18 16:24:12 by jang              #+#    #+#             */
/*   Updated: 2022/12/18 16:26:18 by jang             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

//it is not recommended to put function definitions in header file
//ideally only function declarations.
// .h indicates the file is a header file.

#ifndef FT_H
# define FT_H

void	ft_putchar(char c);
void	ft_swap(int *a, int *b);
void	ft_putstr(char *str);
int		ft_strlen(char *str);
int		ft_strcmp(char *s1, char *s2);

#endif
