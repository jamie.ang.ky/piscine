/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_program_name.c                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jang <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/12/13 20:04:51 by jang              #+#    #+#             */
/*   Updated: 2022/12/14 18:14:10 by jang             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

//create a program that displays it's own name followed by a new line

int	main(int argc, char **argv)
{
	int	j;

	j = 0;
	if (argc >= 1)
	{
		while (argv[0][j] != '\0')
		{
			write(1, &argv[0][j], 1);
			j++;
		}
		write(1, "\n", 1);
	}
	return (0);
}
