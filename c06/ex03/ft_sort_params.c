/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_sort_params.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jang <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/12/13 20:50:36 by jang              #+#    #+#             */
/*   Updated: 2022/12/13 21:28:36 by jang             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

//display arguments sorted by ascii order
//one per line
//don't display argv[0]

#include <unistd.h>

void	ft_putstr(char *str)
{
	int	i;

	i = 0;
	while (str[i] != '\0')
	{
		write(1, &str[i], 1);
		i++;
	}
}

int	main(int argc, char **argv)
{
	int		i;
	char	ascii;

	ascii = ' ';
	while (ascii >= ' ' && ascii <= '~')
	{
		i = 1;
		while (i < argc)
		{
			if (argv[i][0] == ascii)
			{
				ft_putstr(argv[i]);
				write(1, "\n", 1);
			}
			i++;
		}
		ascii++;
	}
}
