/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcmp.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jang <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/12/06 17:03:52 by jang              #+#    #+#             */
/*   Updated: 2022/12/06 17:07:59 by jang             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int	ft_strcmp(char *s1, char *s2)
{
	int	i;

	i = 0;
	while (s1[i] != '\0' || s2[i] != '\0')
	{
		if (s1[i] > s2[i])
			return (1);
		else if (s1[i] < s2[i])
			return (-1);
		i++;
	}
	return (0);
}

/*
#include <stdio.h>

int	ft_strcmp(char *s1, char *s2);

int	main(void)
{
	char	s1[] = "Pretty woman";
	char	s2[] = "Pretty man";
	printf("The strings being compared are %s from s1 and %s from s2.\n", s1, s2);
	if (ft_strcmp(s1, s2) == 0)
		printf("s1 = s2: returned 0.\n");
	if (ft_strcmp(s1, s2) == 1)
		printf("s1 > s2: returned 1.\n");
	if (ft_strcmp(s1, s2) == -1)
		printf("s1 < s2: returned -1.\n");
	return (0);
}
*/
