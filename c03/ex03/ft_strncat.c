/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jang <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/12/07 16:57:01 by jang              #+#    #+#             */
/*   Updated: 2022/12/08 17:58:11 by jang             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

char	*ft_strncat(char *dest, char *src, unsigned int nb)
{
	unsigned int	i;
	unsigned int	j;

	i = 0;
	j = 0;
	while (dest[i] != '\0')
	{
		i++;
	}
	while (src[j] != '\0' && j < nb)
	{
		dest[i] = src[j];
		i++;
		j++;
	}
	dest[i] = '\0';
	return (dest);
}

/*
#include <string.h>
#include <stdio.h>

int	main(void)
{
	unsigned int    nb;

	char dest[90] = "Do you wanna";
	char src[] = " get bubble tea?";
	nb = 3;
	printf("Before: %s\n", dest);
	printf("After: %s\n", ft_strncat(dest, src, nb));
	printf("Original function output: %s\n", dest);
	return (0);
}
*/
