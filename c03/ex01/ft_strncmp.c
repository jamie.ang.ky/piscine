/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncmp.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jang <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/12/06 16:26:46 by jang              #+#    #+#             */
/*   Updated: 2022/12/06 16:32:10 by jang             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int	ft_strncmp(char *s1, char *s2, unsigned int n)
{
	unsigned int	i;

	i = 0;
	while ((s1[i] != '\0' || s2[i] != '\0') && i < n)
	{
		if (s1[i] > s2[i])
			return (1);
		else if (s1[i] < s2[i])
			return (-1);
		i++;
	}
	return (0);
}

/*
#include <string.h>
#include <stdio.h>

int main(void)
{
	unsigned int n;

	n = 8;
	char s1[] = "Pretty piggy";
	char s2[] = "Pretty piglet";
	ft_strncmp(s1, s2, n);
	if (ft_strncmp(s1, s2, n) == 0)
		printf("s1 = s2: returned 0.\n");
	if (ft_strncmp(s1, s2, n) == 1)
		printf("s1 > s2: returned 1.\n");
	if (ft_strncmp(s1, s2, n) == -1)
		printf("s1 < s2: returned -1.\n");
	strncmp(s1, s2, n);
	return (0);
}
*/
