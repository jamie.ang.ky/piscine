/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strstr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jang <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/12/07 17:04:14 by jang              #+#    #+#             */
/*   Updated: 2022/12/07 17:14:34 by jang             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

char	*ft_strstr(char *str, char *to_find)
{
	int	i;
	int	j;

	i = 0;
	j = 0;
	if (to_find[j] == '\0')
		return (str);
	while (str[i] != '\0')
	{
		while (str[i + j] != '\0' && str[i + j] == to_find[j])
		{
			j++;
			if (to_find[j] == '\0')
				return (str + i);
		}
		i++;
		j = 0;
	}
	return (0);
}

/*
#include <stdio.h>
#include <string.h>
int main(void)
{
	char	str[] = "The Ghost of Tsushima";
	char	to_find[] = "of";

	printf("Found '%s' in '%s'.\n", to_find, ft_strstr(str, to_find));
	printf("Original function output: %s\n", strstr(str, to_find));
	return (0);
}
*/
