/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcat.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jang <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/12/07 16:52:54 by jang              #+#    #+#             */
/*   Updated: 2022/12/08 17:59:00 by jang             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

char	*ft_strcat(char *dest, char *src)
{
	int	i;
	int	j;

	i = 0;
	j = 0;
	while (dest[j] != '\0')
	{
		j++;
	}
	while (src[i] != '\0')
	{
		dest[j] = src[i];
		i++;
		j++;
	}
	dest[j] = '\0';
	return (dest);
}

/*
#include <stdio.h>
#include <string.h>
int	main(void)
{
	char dest[90] = "The source is now in ";
	char src[] = "the destination string.";

	printf("Dest before ft_strcat: %s\n", dest);
	ft_strcat(dest, src);
	printf("Dest after ft_strcat: %s\n", dest);
	printf("Original strcat function output: %s\n", strcat(dest, src));
	return (0);
}
*/
