/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_str_is_uppercase.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jang <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/12/02 10:19:45 by jang              #+#    #+#             */
/*   Updated: 2022/12/02 10:28:37 by jang             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

//func returns 1 if uppercase chars
// 0 if any other char

int	ft_str_is_uppercase(char *str)
{
	while (*str != '\0')
	{
		if (*str >= 'A' && *str <= 'Z')
			str++;
		else
			return (0);
	}
	return (1);
}

/*
#include <stdio.h>

int	main(void)
{
	if (ft_str_is_uppercase("ABCDEFG") == 1)
		printf("1 - all uppercase\n");
	if (ft_str_is_uppercase("ABCdefg") == 0)
		printf("0 - some lowercase\n");
	if (ft_str_is_uppercase("ABC1234") == 0)
		printf("0 - some numbers\n");
	if (ft_str_is_uppercase("") == 1)
		printf("1 - str is empty");
	return (0);
}
*/
