/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_str_is_alpha.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jang <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/12/01 17:29:10 by jang              #+#    #+#             */
/*   Updated: 2022/12/05 17:02:36 by jang             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

//function returns 1 if string param contains only alphabets
//return 0 if contains any other characters

#include <unistd.h>

int	ft_str_is_alpha(char *str)
{
	while (*str != '\0' )
	{
		if ((*str > 64 && *str < 91) || (*str > 96 && *str < 123))
		{
			str++;
		}
		else
		{
			return (0);
		}
	}
	return (1);
}

/*
#include <stdio.h>

int	main(void)
{
	if (ft_str_is_alpha("abcdefgHIJK") == 1)
		printf("1\n");
	if (ft_str_is_alpha("12345678") == 0)
		printf("It's a 0 with all numbers\n");
	if (ft_str_is_alpha("abc1234ijk") == 0)
		printf("It's a 0 with a mix of letters and numbers\n");
	if (ft_str_is_alpha("") == 1)
		printf("It is 1 with an empty string\n");
	return (0);
}
*/
