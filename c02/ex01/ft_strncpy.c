/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jang <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/11/30 20:22:14 by jang              #+#    #+#             */
/*   Updated: 2022/12/02 16:57:24 by jang             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

//copies count characters of src to dest
//including \0
//if count is less than or equal to length of src,
//\0 is not appended to copied dest.
//unsigned int = when the value stored is always non-negative: 0/+.

char	*ft_strncpy(char *dest, char *src, unsigned int n)
{
	unsigned int	i;

	i = 0;
	while (src[i] != '\0' && i < n)
	{
		dest[i] = src[i];
		i++;
	}
	while (i < n)
	{
		dest[i] = '\0';
		i++;
	}
	return (dest);
}

/*
#include <stdio.h>

int	main(void)
{
	char	*dest;
	char	k[] = "dodgeball";

	dest = &k[0];
	ft_strncpy(dest, "softball", 3);
	printf("%s\n", dest);
	return (0);
}
*/
