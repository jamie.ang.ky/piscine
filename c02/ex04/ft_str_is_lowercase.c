/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_str_is_lowercase.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jang <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/12/02 09:45:48 by jang              #+#    #+#             */
/*   Updated: 2022/12/02 10:17:05 by jang             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

//func that returns 1 if str only has lowercase alphabetical
//0 if contain any other char

int	ft_str_is_lowercase(char *str)
{
	while (*str != '\0')
	{
		if (*str >= 97 && *str <= 122)
		{
			str++;
		}
		else
		{
			return (0);
		}
	}
	return (1);
}

/*
#include <stdio.h>

int	main(void)
{
	if (ft_str_is_lowercase("abcdefigh") == 1)
	{
		printf("Return value is 1\n");
	}
	if (ft_str_is_lowercase("a12354adfa231A") == 0)
	{
		printf("Return value is 0\n");
	}
	return (0);
}
*/
