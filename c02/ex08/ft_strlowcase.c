/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlowcase.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jang <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/12/02 11:18:06 by jang              #+#    #+#             */
/*   Updated: 2022/12/02 17:15:25 by jang             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

//transform every letter to lowercase.

#include <unistd.h>

char	*ft_strlowcase(char *str)
{
	int	i;

	i = 0;
	while (str[i] != '\0')
	{
		if (str[i] >= 'A' && str[i] <= 'Z')
		{
			str[i] = str[i] + 32;
		}
		i++;
	}
	return (str);
}

/*
#include <stdio.h>

int	main(void)
{
	char	str[] = "ABCDEFGH";

	*str = *ft_strlowcase(str);
	printf("%s", str);
	return (0);
}
*/
