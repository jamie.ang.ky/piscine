/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_str_is_printable.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jang <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/12/02 10:29:21 by jang              #+#    #+#             */
/*   Updated: 2022/12/05 17:31:28 by jang             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

//returns 1 if str has printable char
//0 if any other char

int	ft_str_is_printable(char *str)
{
	int	i;

	i = 0;
	while (str[i] != '\0')
	{
		if (str[i] >= ' ' && str[i] <= '~')
		{
			i++;
		}
		else
		{
			return (0);
		}
	}
	return (1);
}

/*
#include <stdio.h>

int	main(void)
{
	if (ft_str_is_printable("abcdef123#$%") == 1)
		printf("1 - all printable chars\n");
	if (ft_str_is_printable("") == 1)
		printf("1 - str is empty\n");
	if (ft_str_is_printable("1234\n\fabc") == 0)
		printf("0 - non printable chars\n");
	return (0);
}
*/
