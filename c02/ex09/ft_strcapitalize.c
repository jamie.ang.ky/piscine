/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcapitalize.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jang <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/12/05 10:52:52 by jang              #+#    #+#             */
/*   Updated: 2022/12/05 17:15:35 by jang             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

char	*ft_strcapitalize(char *str)
{
	int	i;

	i = 0;
	while (str[i] != '\0')
	{
		if ((str[i] >= 'a' && str[i] <= 'z') && i == 0)
			str[i] = str[i] - 32;
		else if ((str[i] >= 'a' && str[i] <= 'z')
			&& ((str[i - 1] >= 32) && (str[i - 1] <= 47)))
			str[i] = str[i] - 32;
		else if ((str[i] >= 'A' && str[i] <= 'Z') && i != 0
			&& (!(str[i - 1] >= 32) && (str[i - 1] <= 47)))
			str[i] = str[i] + 32;
		i++;
	}
	return (str);
}

/*
#include <stdio.h>
int	main(void)
{
	char	str[] = "Lol mdr Mdr 4242l42";

	printf("%s\n", str);
	*str = *ft_strcapitalize(str);
	printf("%s\n", str);
	return (0);
}
*/
