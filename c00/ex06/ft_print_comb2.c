/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_comb2.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jang <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/11/25 09:41:55 by jang              #+#    #+#             */
/*   Updated: 2022/11/25 09:45:16 by jang             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

//all different combinations
//2 digits
//between 00 and 99
//ascending order.

void ft_write_comb2(char a[2], char b[2])
{
	if (a[0] + a[1] < b[0] + b[1])
	{

		write (1, &a[0], 1);
		write (1, &a[1], 1);
		write (1, &" ", 1);
		write (1, &b[0], 1);
		write (1, &b[1], 1);
		write (1, &", ", 2);
	}
}

void ft_write_b_digits(char a[2], char b[2])
{
	b[0] = '0';
	while (b[0] <='9')
	{
		b[1] = '0';
	        while (b[1] <='9')
		{
			ft_write_comb2(a, b);
			b[1]++;
		}
		//write (1, &"\n", 2);
	        b[0]++;
	}
	a[1]++;

}
void ft_print_comb2(void)
{
	char	a[2];
	char	b[2];

	a[0] = '0';
	while (a[0] <= '9')
	{
		a[1] = '0';
		while (a[1] <= '9')
		{
			ft_write_b_digits(a, b);			
		}
		a[0]++;
	}		
}
/*
int main(void)
{
	ft_print_comb2();
	return (0);
}
*/
