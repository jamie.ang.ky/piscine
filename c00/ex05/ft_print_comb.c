/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_comb.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jang <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/11/24 09:48:36 by jang              #+#    #+#             */
/*   Updated: 2022/11/24 17:11:15 by jang             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

void	ft_print_comb(void);

void	ft_putchar(char j, char k, char l)
{
	write(1, &j, 1);
	write(1, &k, 1);
	write(1, &l, 1);
}

void	ft_print_comb(void)
{
	char	j;
	char	k;
	char	l;

	j = '0';
	while (j <= '7')
	{
		k = j + 1;
		while (k <= '8')
		{
			l = k + 1;
			while (l <= '9')
			{
				ft_putchar(j, k, l);
				if (j != '7' || k != '8' || l != '9')
				{
					write(1, &", ", 2);
				}
				l++;
			}
			k++;
		}
		j++;
	}
}

/*
int main(void)
{
	ft_print_comb();
	return (0);
}
*/
