/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_sqrt.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jang <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/12/13 16:39:11 by jang              #+#    #+#             */
/*   Updated: 2022/12/17 13:47:47 by jang             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int	ft_sqrt(int nb)
{
	int	i;

	i = 1;
	if (nb <= 0)
		return (0);
	while (i * i <= nb && i <= 46340)
	{
		if (i * i == nb)
			return (i);
		i++;
	}
	return (0);
}

/*
#include <stdio.h>
int	main(void)
{
	printf("Sq root of 0 is: %d\n", ft_sqrt(0));
	printf("Sq root of 1 is: %d\n", ft_sqrt(1));
	printf("Sq root of 2 is: %d\n", ft_sqrt(2));
	printf("Sq root of 144 is: %d\n", ft_sqrt(144));
	printf("Sq root of -9 is: %d\n", ft_sqrt(-9));
	printf("Sq root of 944640225 is: %d\n", ft_sqrt(944640225));
	printf("Sq root of 2147483646 is: %d\n", ft_sqrt(2147483646)); //0
	printf("Sq root of 2147395600 is: %d\n", ft_sqrt(2147395600));
}
*/
