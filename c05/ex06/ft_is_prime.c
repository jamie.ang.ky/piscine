/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_is_prime.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jang <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/12/13 17:51:30 by jang              #+#    #+#             */
/*   Updated: 2022/12/13 20:03:37 by jang             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int	ft_is_prime(int nb)
{
	int	i;

	i = 2;
	if (nb < 2)
		return (0);
	while (i < nb && nb <= 2147483647)
	{
		if (nb % i == 0)
			return (0);
		i++;
	}
	return (1);
}

/*
#include<stdio.h>
int	main(void)
{
	printf("1: return %d\n", ft_is_prime(1));
	printf("2: return %d\n", ft_is_prime(2));
	printf("3: return %d\n", ft_is_prime(3));
	printf("4: return %d\n", ft_is_prime(4));
	printf("5: return %d\n", ft_is_prime(5));
	printf("20002: return %d\n", ft_is_prime(20002));
	printf("2147302921: return %d\n", ft_is_prime(2147302921));
}
*/
