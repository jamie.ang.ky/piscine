/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_recursive_power.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jang <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/12/14 16:54:21 by jang              #+#    #+#             */
/*   Updated: 2022/12/14 16:55:39 by jang             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int	ft_recursive_power(int nb, int power)
{
	if (power < 0)
		return (0);
	if (power == 0)
		return (1);
	else
		return (nb * (ft_recursive_power(nb, power - 1)));
}

/*
#include <stdio.h>
int main(void)
{
    printf("3 with power of 1 = %d\n", ft_recursive_power(3, 1));
    printf("3 with power of 3 = %d\n", ft_recursive_power(3, 3));
    printf("3 with power of 0 = %d\n", ft_recursive_power(3, 0));
    printf("3 with power of -3 = %d\n", ft_recursive_power(3, -3));
    printf("2 with power of 6 = %d\n", ft_recursive_power(2, 6));
    return (0);
}
*/
