/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_recursive_factorial.c                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jang <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/12/10 18:02:27 by jang              #+#    #+#             */
/*   Updated: 2022/12/14 16:51:02 by jang             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

//recursive 4,3,2,1

int	ft_recursive_factorial(int nb)
{
	if (nb < 0)
		return (0);
	else if (nb == 0)
		return (1);
	return (nb * (ft_recursive_factorial(nb - 1)));
}

/*
#include <stdio.h>
#include <unistd.h>
int	main(void)
{
	printf("-1! = %d\n", ft_recursive_factorial(-1));
	printf("0! = %d\n", ft_recursive_factorial(0));
	printf("1! = %d\n", ft_recursive_factorial(1));
	printf("4! = %d\n", ft_recursive_factorial(4));
	printf("5! = %d\n", ft_recursive_factorial(5));
	return (0);
}
*/
