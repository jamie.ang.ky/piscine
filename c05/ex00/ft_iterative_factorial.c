/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_iterative_factorial.c                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jang <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/12/10 14:48:20 by jang              #+#    #+#             */
/*   Updated: 2022/12/14 17:01:05 by jang             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

//4,3,2,1

int	ft_iterative_factorial(int nb)
{
	int	i;
	int	res;

	i = 1;
	res = nb;
	if (nb == 0)
		return (1);
	else if (nb < 0)
		return (0);
	while (i <= nb && (nb - i != 0))
	{
		res = res * (nb - i);
		i++;
	}
	return (res);
}

/*
#include <stdio.h>
#include <unistd.h>
int	main(void)
{
	printf("0! = %d\n",ft_iterative_factorial(0));
	printf("1! = %d\n",ft_iterative_factorial(1));
	printf("2! = %d\n",ft_iterative_factorial(2));
	printf("3! = %d\n",ft_iterative_factorial(3));
	printf("4! = %d\n",ft_iterative_factorial(4));
	printf("5! = %d\n",ft_iterative_factorial(5));
	printf("-3! = %d\n",ft_iterative_factorial(-3));
	return (0);
}
*/
