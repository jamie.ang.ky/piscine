/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_find_next_prime.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jang <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/12/15 18:28:51 by jang              #+#    #+#             */
/*   Updated: 2022/12/17 14:12:40 by jang             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

//return the next prime number greater or equal to the number
//given as an argument

int	ft_is_prime(int nb)
{
	int	i;

	i = 2;
	if (nb < 2)
		return (0);
	while (i < nb && nb <= 2147483647)
	{
		if (nb % i == 0)
			return (0);
		i++;
	}
	return (1);
}

int	ft_find_next_prime(int nb)
{
	int	j;

	if (nb <= 1)
		return (2);
	j = nb;
	while (ft_is_prime(j) == 0)
		j++;
	return (j);
}

/*
#include <stdio.h>
int	main(void)
{
	int x;
	x = ft_find_next_prime(0);
	printf("%d\n", x);
	x = ft_find_next_prime(2);
	printf("%d\n", x);
	x = ft_find_next_prime(1);
	printf("%d\n", x);
	x = ft_find_next_prime(64);
	printf("%d\n", x);
	return (0);
}
*/
