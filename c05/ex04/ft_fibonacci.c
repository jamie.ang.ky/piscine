/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_fibonacci.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jang <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/12/14 16:56:02 by jang              #+#    #+#             */
/*   Updated: 2022/12/14 16:59:17 by jang             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

//fibonacci seq: each number is the sum of 2 preceding numbers
// eg, 0, 1, 1, 2, 3, 5, 8

int	ft_fibonacci(int index)
{
	if (index < 0)
		return (-1);
	else if (index == 0)
		return (0);
	else if (index == 1)
		return (1);
	return (ft_fibonacci(index - 1) + ft_fibonacci(index - 2));
}

/*
#include <stdio.h>
int	main(void)
{
	printf("Index -1 = %d\n", ft_fibonacci(-1));
	printf("Index 0 = %d\n", ft_fibonacci(0));
	printf("Index 1 = %d\n", ft_fibonacci(1));
	printf("Index 2 = %d\n", ft_fibonacci(2));
	printf("Index 3 = %d\n", ft_fibonacci(3));
	printf("Index 4 = %d\n", ft_fibonacci(4));
	return (0);
}
*/
