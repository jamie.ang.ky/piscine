/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_iterative_power.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jang <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/12/14 16:42:18 by jang              #+#    #+#             */
/*   Updated: 2022/12/14 17:01:45 by jang             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int	ft_iterative_power(int nb, int power)
{
	int	res;

	res = nb;
	if (power < 0)
		return (0);
	if (power == 0)
		return (1);
	while (power > 1 && nb != 0)
	{
		res = res * nb;
		power--;
	}
	return (res);
}

/*
#include <stdio.h>
int	main(void)
{
	printf("3 with power of 1 = %d\n", ft_iterative_power(3, 1));
	printf("3 with power of 3 = %d\n", ft_iterative_power(3, 3));
	printf("3 with power of 0 = %d\n", ft_iterative_power(3, 0));
	printf("3 with power of -3 = %d\n", ft_iterative_power(3, -3));
	printf("2 with power of 6 = %d\n", ft_iterative_power(2, 6));
	return (0);
}
*/
